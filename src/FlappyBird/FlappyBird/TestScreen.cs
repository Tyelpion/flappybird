﻿namespace FlappyBird
{
	using GmCore;
	using GmCore.Actions;
	using Microsoft.Xna.Framework;

	public class TestScreen : Screen
	{
		private float speed;
		private BirdTwo player;
		private BirdTwo entity;

		public override void Start()
		{
			this.speed = 2.0f;
			this.AddChild(this.player = new BirdTwo(new Vector2(50, 50), ResourceManager.GetTextureRegion("bird.yellow"), 100, true));
			this.AddChild(this.entity = new BirdTwo(new Vector2(100, 400), ResourceManager.GetTextureRegion("bird.yellow"), 100, true));
		}

		public override void End()
		{
		}

		public override void UpdateLogic(GameTime gameTime)
		{
			this.player.Update(gameTime);
			this.entity.Update(gameTime);
			//this.playerAction.Update(gameTime);

			if (this.player.X < (this.player.Width / 2f) || this.player.X > (288 - this.player.Width / 2f))
				this.player.ReflectVertical();

			if (this.player.Y < (this.player.Height / 2f) || this.player.Y > (512 - this.player.Height / 2f))
				this.player.ReflectHorizontal();

			if (this.entity.X < (this.entity.Width / 2f) || this.entity.X > (288 - this.entity.Width / 2f))
				this.entity.ReflectVertical();

			if (this.entity.Y < (this.entity.Height / 2f) || this.entity.Y > (512 - this.entity.Height / 2f))
				this.entity.ReflectHorizontal();

			if (this.player.Intersects(this.entity))
			{
				this.player.SwapVelocity(this.entity);
			}
		}
	}
}