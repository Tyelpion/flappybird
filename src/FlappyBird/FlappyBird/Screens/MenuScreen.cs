﻿namespace FlappyBird
{
	using GmCore;
	using GmCore.Actions;
	using GmCore.Tweenings;
	using Microsoft.Xna.Framework;

	internal class MenuScreen : MainScreen
	{
		private Button playButton;
		private Button rankButton;
		private ImageSprite flappybird;
		private ImageSprite watermark;
		private Action fadeInAction;
		private Action fadeOutAction;

		public override void Start()
		{
			base.Start();

			this.infoLayer.AddChild(this.flappybird = new ImageSprite(new Vector2(this.ScreenWidth / 2f, this.floor / 2f - 80f), Alignment.MiddleCenter, ResourceManager.GetTextureRegion("flappybird")));
			this.infoLayer.AddChild(this.player);
			this.infoLayer.AddChild(this.watermark = new ImageSprite(new Vector2(this.ScreenWidth / 2f, this.ScreenHeight - 50f), Alignment.MiddleCenter, ResourceManager.GetTextureRegion("watermark")));
			this.infoLayer.AddChild(this.playButton = new SpringButton(this, new Vector2(15f, this.floor), Alignment.BottomLeft, ResourceManager.GetTextureRegion("button.play")));
			this.infoLayer.AddChild(this.rankButton = new SpringButton(this, new Vector2(this.ScreenWidth - 15f, this.floor), Alignment.BottomRight, ResourceManager.GetTextureRegion("button.rank")));

			this.fadeInAction = new ColorChangeAction(Color.DarkGray, Color.White, 10, new CubicTweening(TweeningFunction.EaseIn),
				this.background, this.platform, this.playButton, this.rankButton, this.flappybird, this.watermark, this.player);
			this.fadeInAction.Initialize();

			this.fadeOutAction = new ColorChangeAction(Color.DarkGray, Color.Black, 0.2f, new CubicTweening(TweeningFunction.EaseOut),
				this.background, this.platform, this.playButton, this.rankButton, this.flappybird, this.watermark, this.player);
			this.fadeOutAction.Stop();

			this.playButton.Click += playButton_Click;
		}

		public override void End()
		{
			this.playButton.Click -= playButton_Click;
			base.End();
		}

		public override void UpdateLogic(GameTime gameTime)
		{
			this.player.Update(gameTime);
			this.fadeInAction.Update();
			this.fadeOutAction.Update(gameTime);

			this.playButton.State = ResourceManager.IsGameActive() ? GmButtonState.Up : GmButtonState.Disable;

			if (this.fadeOutAction.Finished)
			{
				ScreenManager.Switch(new PlayingScreen());
			}

			base.UpdateLogic(gameTime);
		}

		private void playButton_Click(ButtonEventsArgs args)
		{
			ResourceManager.GetSound("button").Play();
			this.fadeOutAction.Initialize();
		}
	}
}