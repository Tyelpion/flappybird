﻿namespace FlappyBird
{
	using GmCore;
	using Microsoft.Xna.Framework;

	internal class MainScreen : Screen
	{
		protected ImageSprite background;
		protected ImageSprite platform;
		protected Sprite infoLayer;
		protected Sprite mainLayer;
		protected Bird player;
		protected bool playerFloating;
		protected int floor;
		protected int roof;
		protected float speed;
		private float playerTop;
		private float playerBottom;
		private float step;

		protected MainScreen()
			: base(ResourceManager.ScreenWidth, ResourceManager.ScreenHeight)
		{
		}

		public override void Start()
		{
			this.AddChild(this.background = new ImageSprite(Vector2.Zero, Alignment.TopLeft, ResourceManager.GetTextureRegion("background.day")));
			this.background.AddChild(this.mainLayer = new Sprite());
			this.background.AddChild(this.platform = new ImageSprite(new Vector2(0, this.ScreenHeight), Alignment.BottomLeft, ResourceManager.GetTextureRegion("platform")));
			this.background.AddChild(this.infoLayer = new Sprite());
			this.floor = this.ScreenHeight - this.platform.Height;
			this.roof = 0;
			this.speed = 2f;

			this.player = new Bird(new Vector2(this.ScreenWidth / 2f, this.ScreenHeight / 2f), ResourceManager.GetTextureRegion("bird.yellow"), ResourceManager.FrameTime, true);
			this.playerFloating = true;
			this.playerTop = this.player.Y - 4;
			this.playerBottom = this.player.Y;
			this.step = 0.2f;
		}

		public override void End()
		{
			this.background.RemoveChild(this.platform);
		}

		public override void UpdateLogic(GameTime gameTime)
		{
			if (this.platform.X <= -24f)
			{
				this.platform.GoTo(-this.speed, null);
			}
			else
			{
				this.platform.MoveLeft(this.speed);
			}

			if (this.playerFloating)
			{
				if (this.player.Y < this.playerTop)
				{
					step = System.Math.Abs(step);
				}
				if (this.player.Y > this.playerBottom)
				{
					step = -System.Math.Abs(step);
				}
				this.player.Move(0, step);
			}
		}
	}
}