﻿namespace FlappyBird
{
	using GmCore;
	using Microsoft.Xna.Framework;

	internal class LogoScreen : Screen
	{
		private int elapsedTime;

		public override void Start()
		{
			this.elapsedTime = 0;
			this.AddChild(new ImageSprite(Vector2.Zero, ResourceManager.GetTextureRegion("splash")));
		}

		public override void End()
		{
		}

		public override void UpdateLogic(GameTime gameTime)
		{
			this.elapsedTime += gameTime.ElapsedGameTime.Milliseconds;

			if (this.elapsedTime > 2000)
			{
				ScreenManager.Switch(new MenuScreen());
			}
		}
	}
}