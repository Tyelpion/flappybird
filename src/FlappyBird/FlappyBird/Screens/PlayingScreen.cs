﻿namespace FlappyBird
{
	using GmCore;
	using GmCore.Tweenings;
	using GmCore.Actions;
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Input;
	using System.Collections.Generic;

	internal class PlayingScreen : MainScreen
	{
		private enum GameState
		{
			Start, Playing, End
		}

		#region Fields

		private Sprite pipeLayer;
		private Sprite gameover;
		private Sprite getready;
		private Sprite taptap;
		private Sprite scoreboard;
		private Button okButton;
		private Button shareButton;
		private ImageSprite medal;
		private ImageSprite newScore;
		private AnimatedSprite sparkle;
		private Pipe[] pipes;
		private bool[] passed;
		private NumberSprite scoreNumber;
		private NumberSprite finalScoreNumber;
		private NumberSprite highestScoreNumber;
		private float offsetX;
		private int minRand;
		private int maxRand;
		private int score;
		private double elapsedTime;
		private MouseState previous;
		private GameState playState;
		private Action fadeInAction;
		private Action fadeOutAction;
		private Action collisionAction;
		private Action scoreboardAction;
		private Action scoreFadeOutAction;
		private Action gameoverFadeInAction;

		#endregion Fields

		public PlayingScreen()
		{
			this.pipes = new Pipe[3];
		}

		public override void Start()
		{
			base.Start();
			this.InitMainGame();
			this.InitPipes();
			this.InitActions();

			// Adjust offsetX to catch up the platform.
			this.offsetX -= this.speed;
		}

		public override void End()
		{
			base.End();
		}

		public override void UpdateLogic(GameTime gameTime)
		{
			this.fadeInAction.Update();
			this.player.Update(gameTime);
			this.sparkle.Update(gameTime);

			this.UpdateMouse(gameTime);

			if (this.playState == GameState.Playing || this.playState == GameState.End)
			{
				this.collisionAction.Update(gameTime);

				if (!this.player.Stopped)
				{
					this.UpdatePlayer(gameTime);
				}

				if (this.playState == GameState.Playing && this.player.Alive)
				{
					this.UpdatePipes(gameTime);
				}

				if (this.playState == GameState.End)
				{
					this.UpdateEnd(gameTime);
				}
			}

			if (this.player.Alive)
			{
				base.UpdateLogic(gameTime);
			}
		}

		private void InitMainGame()
		{
			this.score = 0;
			this.playState = GameState.Start;

			this.mainLayer.AddChild(this.pipeLayer = new Sprite());
			this.mainLayer.AddChild(this.player);
			this.player.GoTo(85f, null);
			this.infoLayer.AddChild(this.getready = new ImageSprite(new Vector2(this.ScreenWidth / 2f, 100f), Alignment.TopCenter, ResourceManager.GetTextureRegion("getready")));
			this.infoLayer.AddChild(this.taptap = new ImageSprite(new Vector2(this.ScreenWidth / 2f, this.ScreenHeight / 2f), Alignment.MiddleCenter, ResourceManager.GetTextureRegion("taptap")));
			this.gameover = new ImageSprite(new Vector2(this.ScreenWidth / 2f, 100f), Alignment.TopCenter, ResourceManager.GetTextureRegion("gameover"));
			this.scoreboard = new ImageSprite(new Vector2(this.ScreenWidth / 2f, this.ScreenHeight), Alignment.TopCenter, ResourceManager.GetTextureRegion("scoreboard"));
			this.scoreboard.AddChild(this.medal = new ImageSprite(new Vector2(32 - this.scoreboard.Width / 2f, 43), Alignment.TopLeft, ResourceManager.GetTextureRegion("medal.gold")));
			this.newScore = new ImageSprite(new Vector2(this.scoreboard.Width - 80, 38), Alignment.TopRight, ResourceManager.GetTextureRegion("new"));

			this.sparkle = new AnimatedSprite(new Vector2(68 - this.scoreboard.Width / 2f, 50), Alignment.TopLeft, ResourceManager.GetTextureRegion("sparkle"), 250, true);

			this.shareButton = new SpringButton(this, new Vector2(this.ScreenWidth - 20f, this.floor - 20f), Alignment.BottomRight, ResourceManager.GetTextureRegion("button.share"));
			this.okButton = new SpringButton(this, new Vector2(20f, this.floor - 20f), Alignment.BottomLeft, ResourceManager.GetTextureRegion("button.ok"));
			this.okButton.Click += okButton_Click;

			this.infoLayer.AddChild(this.scoreNumber = new NumberSprite(new Vector2(this.ScreenWidth / 2f, 50), Alignment.MiddleCenter, ResourceManager.GetTextureRegion("number.large"), this.score));
			this.scoreboard.AddChild(this.finalScoreNumber = new NumberSprite(new Vector2(this.scoreboard.Width - 28, 32), Alignment.TopRight, ResourceManager.GetTextureRegion("number.medium")));
			this.scoreboard.AddChild(this.highestScoreNumber = new NumberSprite(new Vector2(this.scoreboard.Width- 28, 74), Alignment.TopRight, ResourceManager.GetTextureRegion("number.medium")));
			// Adjust floor and roof to improve visual effects.
			this.floor += (int) this.speed;
			this.roof += this.player.Height / 2;
		}

		private void InitPipes()
		{
			var pipedown = ResourceManager.GetTextureRegion("pipe.green.down");
			var pipeup = ResourceManager.GetTextureRegion("pipe.green.up");

			this.maxRand = -50;
			this.minRand = -pipedown.Height + 50;

			// First pipe with a long pause.
			this.pipes[0] = new Pipe(new Vector2(this.ScreenWidth * 1.6f, ResourceManager.Random.Next(this.minRand, this.maxRand)), pipedown, pipeup);
			this.offsetX = (this.ScreenWidth + this.pipes[0].Width) / 2f - (this.pipes[0].Width / 4f);

			for (var i = 1; i < this.pipes.Length; ++i)
			{
				this.pipes[i] = new Pipe(new Vector2(this.pipes[i - 1].X + this.offsetX, ResourceManager.Random.Next(this.minRand, this.maxRand)), pipedown, pipeup);
			}

			this.passed = new bool[this.pipes.Length];
			for (var i = 1; i < this.passed.Length; ++i)
			{
				this.passed[i] = false;
			}
		}

		private void InitActions()
		{
			this.fadeInAction = new ColorChangeAction(Color.DarkGray, Color.White, 10, new CubicTweening(TweeningFunction.EaseIn),
				this.background, this.platform, this.player, this.taptap, this.getready, this.scoreNumber);
			this.fadeInAction.Initialize();

			// Fading-Out action
			var fadeOutSprites = new Deque<Sprite>(
				this.pipes,
				this.background,
				this.platform,
				this.player,
				this.gameover,
				this.scoreboard,
				this.okButton,
				this.shareButton,
				this.finalScoreNumber,
				this.highestScoreNumber,
				this.medal,
				this.newScore,
				this.sparkle);
			this.fadeOutAction = new ColorChangeAction(Color.DarkGray, Color.Black, 0.2f, new CubicTweening(TweeningFunction.EaseOut), fadeOutSprites.ToArray());
			this.fadeOutAction.Stop();

			// Collision action
			var collisionSprites = new Deque<Sprite>(this.pipes, this.background, this.player, this.scoreNumber);
			this.collisionAction = new OpacityChangeAction(0.9f, 1f, 0.5f, new ElasticTweening(TweeningFunction.EaseOut), collisionSprites.ToArray());
			this.collisionAction.Stop();

			// Scoreboard action
			this.scoreboardAction = new DelayAction(new YTranslationAction(this.ScreenHeight, 200f, 0.5f, new ExponentialTweening(TweeningFunction.EaseOut)), 0.5f, this.scoreboard);
			this.scoreboardAction.Initialize();
			this.scoreboardAction.Stop();

			// Score fade-out and Game Over fade-in
			this.scoreFadeOutAction = new OpacityChangeAction(1f, 0.8f, 20, new ExponentialTweening(TweeningFunction.EaseIn), this.scoreNumber);
			this.scoreFadeOutAction.Initialize();
			this.scoreFadeOutAction.Stop();

			this.gameoverFadeInAction = new SimultaneousAction(new Action[]{
					new OpacityChangeAction(0.8f, 1f, 0.4f, new LinearTweening(TweeningFunction.EaseIn)),
					new SequenceAction(new Action[]{
						new YTranslationAction(this.gameover.Y, this.gameover.Y - 7.5f, 0.2f, new ExponentialTweening(TweeningFunction.EaseOut)),
						new YTranslationAction(this.gameover.Y - 8, this.gameover.Y, 0.2f, new ExponentialTweening(TweeningFunction.EaseOut))
					})}, this.gameover);
			this.gameoverFadeInAction.Initialize();
			this.gameoverFadeInAction.Stop();
		}

		private void UpdateMouse(GameTime gameTime)
		{
			var mouse = Mouse.GetState();

			if (this.Contains(mouse))
			{
				if (ResourceManager.IsGameActive() && mouse.LeftButton == ButtonState.Pressed && this.previous.LeftButton == ButtonState.Released)
				{
					switch (this.playState)
					{
						case GameState.Start:
							this.infoLayer.RemoveChild(this.getready);
							this.infoLayer.RemoveChild(this.taptap);
							this.playerFloating = false;
							for (var i = 0; i < this.pipes.Length; ++i)
							{
								this.pipeLayer.AddChild(this.pipes[i]);
							}
							this.playState = GameState.Playing;
							goto case GameState.Playing;

						case GameState.Playing:
							ResourceManager.GetSound("flap").Play();
							this.player.MoveUp(6f);
							break;

						default:
							break;
					}
				}
			}

			this.previous = mouse;
		}

		private void UpdatePlayer(GameTime gameTime)
		{
			this.player.MoveDown(0.3f);

			if (this.player.Y > this.floor)
			{
				this.player.Stop(this.floor);
				if (this.player.Alive)
				{
					this.InvokeDeathEffects();
				}
			}
			else if (this.player.Y < this.roof)
			{
				this.player.GoTo(null, this.roof);
				this.player.MoveDown(3f);
			}
		}

		private void UpdatePipes(GameTime gameTime)
		{
			for (var i = 0; i < this.pipes.Length; ++i)
			{
				// If pipes go out of screen, reset their positions.
				if (this.pipes[i].X < -this.pipes[i].Width)
				{
					var j = (i == 0) ? this.pipes.Length - 1 : i - 1;

					this.pipes[i].GoTo(this.pipes[j].X + this.offsetX, ResourceManager.Random.Next(this.minRand, this.maxRand));
					this.passed[i] = false;
				}

				// Check for collision.
				//if (this.pipes[i].Intersects(this.player))
				//{
				//	this.InvokeDeathEffects();
				//}

				// If player passed through the current pipe.
				if (!this.passed[i] && this.player.X > (this.pipes[i].X + this.pipes[i].Width / 2f))
				{
					this.scoreNumber.Value = ++this.score;
					ResourceManager.GetSound("point").Play();
					this.passed[i] = true;
				}

				this.pipes[i].MoveLeft(this.speed);
				this.pipes[i].Update(gameTime);
			}
		}

		private void UpdateEnd(GameTime gameTime)
		{
			this.okButton.State = ResourceManager.IsGameActive() ? GmButtonState.Up : GmButtonState.Disable;

			if (this.collisionAction.Finished)
			{
				this.scoreFadeOutAction.Start();
				this.scoreFadeOutAction.Update();

				if (this.scoreFadeOutAction.Finished)
				{
					this.infoLayer.RemoveChild(this.scoreNumber);
					this.gameoverFadeInAction.Start();
					this.gameoverFadeInAction.Update(gameTime);
					this.infoLayer.AddChild(this.gameover);

					this.elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
					this.infoLayer.AddChild(this.scoreboard);

					if (this.elapsedTime > 100)
					{
						this.scoreboardAction.Start();
						this.scoreboardAction.Update(gameTime);

						if (this.scoreboardAction.Finished)
						{
							this.infoLayer.AddChild(this.okButton);
							this.infoLayer.AddChild(this.shareButton);
						}
					}
				}
			}

			this.fadeOutAction.Update(gameTime);

			if (this.fadeOutAction.Finished)
			{
				ScreenManager.Switch(new MenuScreen());
			}
		}

		private void InvokeDeathEffects()
		{
			ResourceManager.GetSound("hit").Play();
			this.SetScore();
			this.player.Die();
			this.collisionAction.Initialize();
			this.playState = GameState.End;
		}

		private void SetScore()
		{
			if (this.score > ResourceManager.HighestScore)
			{
				this.scoreboard.AddChild(this.newScore);
			}

			if (this.score >= 4)
			{
				this.SetMedal();
			}
			else
			{
				this.scoreboard.RemoveChild(this.medal);
			}

			ResourceManager.SetHighestScore(this.score);
			this.finalScoreNumber.Value = this.score;
			this.highestScoreNumber.Value = ResourceManager.HighestScore;
			this.finalScoreNumber.GoTo(new Vector2(this.scoreboard.Width / 2 - 27, 34));
			this.highestScoreNumber.GoTo(new Vector2(this.scoreboard.Width / 2 - 27, 76));
		}

		private void SetMedal()
		{
			var x = ResourceManager.HighestScore / 4f;
			var hasSparkles = true;

			if (this.score <= (x * 4f))
			{
				if (this.score <= (x * 3f))
				{
					if (this.score <= (x * 2f))
					{
						if (this.score <= x)
						{
							this.scoreboard.RemoveChild(this.medal);
							hasSparkles = false;
						}
						else
						{
							this.medal.ChangeTextureRegion(ResourceManager.GetTextureRegion("medal.tin"));
							hasSparkles = false;
						}
					}
					else
					{
						this.medal.ChangeTextureRegion(ResourceManager.GetTextureRegion("medal.bronze"));
						hasSparkles = false;
					}
				}
				else
				{
					this.medal.ChangeTextureRegion(ResourceManager.GetTextureRegion("medal.silver"));
				}
			}

			if (hasSparkles)
			{
				this.scoreboard.AddChild(this.sparkle);
			}
		}

		private void okButton_Click(ButtonEventsArgs args)
		{
			ResourceManager.GetSound("button").Play();
			this.fadeOutAction.Initialize();
		}
	}
}