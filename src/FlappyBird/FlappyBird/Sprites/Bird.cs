﻿namespace FlappyBird
{
	using GmCore;
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	internal class Bird : AnimatedSprite
	{
		private bool alive;
		private bool stopFalling;
		private float velocity;
		private double dt;
		private int delayTime;
		private float angleScale;
		private bool rotateUp;
		private Vector2 birdPos;
		private Vector2 birdOffset;

		public Bird(Vector2 position, TextureRegion textureRegion, int frameTime, bool looping)
			: base(position, Alignment.MiddleCenter, textureRegion, frameTime, looping)
		{
			this.stopFalling = false;
			this.alive = true;
			this.velocity = 0;
			this.rotateUp = false;
			
			// The new boundary is a square with sides are equal to the diagonal of the texture.
			var squareSize = System.Math.Max(this.Width, this.Height); //(int) System.Math.Sqrt(this.Width * this.Width + this.Height * this.Height);
			this.birdOffset = new Vector2((squareSize - this.Width) / 2f, (squareSize - this.Height) / 2f);
			this.Boundary = new Rectangle((int) this.X, (int) this.Y, squareSize, squareSize);
		}

		public bool Alive
		{
			get { return this.alive; }
		}

		public bool Stopped
		{
			get { return this.stopFalling; }
		}

		protected override void UpdateX()
		{
			base.UpdateX();
			this.birdPos.X = this.X + this.birdOffset.X;
		}

		protected override void UpdateY()
		{
			base.UpdateY();
			this.birdPos.Y = this.Y + this.birdOffset.Y;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(this.textureRegion.Texture, this.birdPos, this.textureRegion.Region, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);
		}

		public override void Update(GameTime gameTime)
		{
			if (!this.stopFalling)
			{
				this.dt += gameTime.ElapsedGameTime.TotalMilliseconds;
				this.delayTime += gameTime.ElapsedGameTime.Milliseconds;
				this.Y += this.velocity;

				if (this.rotateUp)
				{
					if (this.Angle > -0.2f)
					{
						this.Angle -= 0.1f;
					}
					else
					{
						this.rotateUp = false;
					}
				}
			}

			base.Update(gameTime);
		}

		public void Die()
		{
			this.Active = false;
			this.alive = false;
		}

		public void Stop()
		{
			this.stopFalling = true;
		}

		public void Stop(float floor)
		{
			this.stopFalling = true;
			this.Y = floor - this.Height + this.Height / 4;
		}

		public override void MoveUp(float value)
		{
			if (this.alive)
			{
				this.velocity = -value;
				this.rotateUp = true;
				this.delayTime = 0;
				this.angleScale = 0f;
			}
		}

		public override void MoveDown(float value)
		{
			if (!this.stopFalling)
			{
				this.velocity += value;

				if (this.delayTime > 400)
				{
					this.angleScale += 0.0015f * this.Y;

					if (this.Angle < 1.5f)
					{
						this.Angle += 0.00004f * delayTime * angleScale;
					}
				}
			}
		}
	}
}