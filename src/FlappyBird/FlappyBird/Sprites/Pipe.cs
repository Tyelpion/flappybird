﻿namespace FlappyBird
{
	using GmCore;
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	internal class Pipe : Sprite
	{
		private TextureRegion above;
		private TextureRegion below;
		private int distance;
		private Rectangle aboveBoundary;
		private Rectangle belowBoundary;
		private Vector2 abovePosition;
		private Vector2 belowPosition;

		public Pipe(Vector2 position, TextureRegion above, TextureRegion below)
			: this(position, above, below, 100)
		{
		}

		public Pipe(Vector2 position, TextureRegion above, TextureRegion below, int space)
			: base(position)
		{
			if (above != null && below != null)
			{
				this.above = above;
				this.below = below;
				this.distance = this.above.Height + space;
				this.abovePosition = position;
				this.belowPosition = new Vector2(abovePosition.X, abovePosition.Y + this.distance);
				this.SetBoundaries();
			}
			else
			{
				throw new System.ArgumentNullException((above == null) ? "above" : "below");
			}
		}

		public override int Width
		{
			get
			{
				return this.above.Width;
			}
		}

		public override int Height
		{
			get
			{
				return this.distance + this.below.Height;
			}
		}

		public override bool Intersects(Sprite sprite)
		{
			return (sprite.Boundary.Intersects(this.aboveBoundary) || sprite.Boundary.Intersects(this.belowBoundary));
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(this.above.Texture, this.abovePosition, this.above.Region, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);
			spriteBatch.Draw(this.below.Texture, this.belowPosition, this.below.Region, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);

			base.Draw(spriteBatch);
		}

		protected override void UpdateX()
		{
			this.abovePosition.X = this.X;
			this.aboveBoundary.X = (int) this.abovePosition.X;

			this.belowPosition.X = this.X;
			this.belowBoundary.X = (int) this.belowPosition.X;
		}

		protected override void UpdateY()
		{
			this.abovePosition.Y = this.Y;
			this.aboveBoundary.Y = (int) this.abovePosition.Y;

			this.belowPosition.Y = this.Y + this.distance;
			this.belowBoundary.Y = (int) this.belowPosition.Y;
		}

		private void SetBoundaries()
		{
			this.aboveBoundary.X = (int) this.X;
			this.aboveBoundary.Y = (int) this.Y;
			this.aboveBoundary.Width = this.above.Region.Width;
			this.aboveBoundary.Height = this.above.Region.Height;

			this.belowBoundary.X = (int) this.X;
			this.belowBoundary.Y = (int) this.Y + this.distance;
			this.belowBoundary.Width = this.below.Region.Width;
			this.belowBoundary.Height = this.below.Region.Height;
		}
	}
}