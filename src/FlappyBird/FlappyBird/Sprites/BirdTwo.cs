﻿namespace FlappyBird
{
	using GmCore;
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	public class BirdTwo : AnimatedSprite
	{
		private bool alive;
		private Vector2 velocity;
		private bool rotateUp;
		private Vector2 birdPos;
		private Vector2 birdOffset;

		public BirdTwo(Vector2 position, TextureRegion textureRegion, int frameTime, bool looping)
			: base(position, Alignment.MiddleCenter, textureRegion, frameTime, looping)
		{
			this.alive = true;
			this.velocity = new Vector2(1f, 1.2f);
			this.rotateUp = false;
			
			// The new boundary is a square with sides are equal to the diagonal of the texture.
			var squareSize = System.Math.Max(this.Width, this.Height); //(int) System.Math.Sqrt(this.Width * this.Width + this.Height * this.Height);
			this.birdOffset = new Vector2((squareSize - this.Width) / 2f, (squareSize - this.Height) / 2f);
			this.Boundary = new Rectangle((int) this.X, (int) this.Y, squareSize, squareSize);
		}

		public bool Alive
		{
			get { return this.alive; }
		}

		protected override void UpdateX()
		{
			base.UpdateX();
			this.birdPos.X = this.X + this.birdOffset.X;
		}

		protected override void UpdateY()
		{
			base.UpdateY();
			this.birdPos.Y = this.Y + this.birdOffset.Y;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(this.textureRegion.Texture, this.birdPos, this.textureRegion.Region, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);
		}

		public override void Update(GameTime gameTime)
		{
			this.X += velocity.X;
			this.Y += velocity.Y;

			base.Update(gameTime);
		}

		public void Die()
		{
			this.Active = false;
			this.alive = false;
		}

		public void ReflectVertical()
		{
			this.velocity.X *= -1;
		}

		public void ReflectHorizontal()
		{
			this.velocity.Y *= -1;
		}
	}
}
