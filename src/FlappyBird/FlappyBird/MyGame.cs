namespace FlappyBird
{
	using GmCore;
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using System.IO;

	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class MyGame : Microsoft.Xna.Framework.Game
	{
		private GraphicsDeviceManager graphics;
		private SpriteBatch spriteBatch;

		public MyGame()
		{
			this.graphics = new GraphicsDeviceManager(this);
			this.graphics.PreferredBackBufferWidth = 288;
			this.graphics.PreferredBackBufferHeight = 512;
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here
			this.IsMouseVisible = true;
			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			// TODO: use this.Content to load your game content here
			var debug = new Texture2D(this.GraphicsDevice, 1, 1);
			debug.SetData(new Color[] { Color.White });

			ResourceManager.AddTexture("debug", debug);
			ResourceManager.LoadResources(this.Content, "Content\\Resources.xml");
			ResourceManager.SetGameWindow(this.Window);
			ResourceManager.FrameTime = 110;
			ScreenManager.Switch(new TestScreen());
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
			this.SaveScore();
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			// TODO: Add your update logic here
			ScreenManager.Update(gameTime);

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			// TODO: Add your drawing code here
			spriteBatch.Begin();
			ScreenManager.Draw(spriteBatch);
			spriteBatch.End();

			base.Draw(gameTime);
		}

		private void LoadScore(string path = "Content\\score.data")
		{
			if (File.Exists(path))
			{
				using (var file = File.Open(path, FileMode.Open))
				{
					using (var reader = new BinaryReader(file))
					{
						ResourceManager.SetHighestScore(reader.ReadInt32());
					}
				}
			}
			else
			{
				SaveScore();
			}
		}

		private void SaveScore(string path = "Content\\score.data")
		{
			using (var file = File.Open(path, FileMode.OpenOrCreate))
			{
				using (var writer = new BinaryWriter(file))
				{
					writer.Flush();
					writer.Write(ResourceManager.HighestScore);
				}
			}
		}
	}
}