﻿namespace GmCore.Actions
{
	using Microsoft.Xna.Framework;
	using System.Collections.Generic;

	public class SequenceAction : Action
	{
		private Deque<Action> actions;
		private int current;

		public SequenceAction(IList<Action> actions, params Sprite[] sprites)
			: base(sprites)
		{
			if (actions != null)
			{
				this.current = 0;
				this.actions = new Deque<Action>(actions);
			}
			else
			{
				throw new System.ArgumentNullException("actions");
			}
		}

		public override bool Finished
		{
			get { return this.current == this.actions.Count; }
		}

		public override void Initialize(params Sprite[] sprites)
		{
			base.Initialize(sprites);
			this.current = 0;
			foreach (var action in actions)
			{
				action.Initialize(this.sprites);
			}
		}

		public override void Update()
		{
			if (this.Running && this.current < this.actions.Count)
			{
				this.actions[this.current].Update();

				if (this.actions[this.current].Finished)
				{
					this.current += 1;
				}
			}
		}

		public override void Update(GameTime gameTime)
		{
			if (this.Running && this.current < this.actions.Count)
			{
				this.actions[this.current].Update(gameTime);

				if (this.actions[this.current].Finished)
				{
					this.current += 1;
				}
			}
		}

		public void AddAction(Action action)
		{
			if (!this.actions.Contains(action))
			{
				this.actions.AddBack(action);
			}
		}

		public void RemoveAction(Action action)
		{
			if (this.actions.Contains(action))
			{
				this.actions.Remove(action);
			}
		}
	}
}