﻿namespace GmCore.Actions
{
	public class LoopAction : Action
	{
		private Action action;
		private int times;

		public LoopAction(Action action, int times, params Sprite[] sprites)
			: base(sprites)
		{
			if (action != null)
			{
				this.action = action;
				this.times = times;
			}
			else
			{
				throw new System.ArgumentNullException("action");
			}
		}

		public override bool Finished
		{
			get { return this.times >= 0 && this.Count == this.times; }
		}

		public override void Initialize(params Sprite[] sprites)
		{
			base.Initialize(sprites);
			this.action.Initialize(this.sprites);
		}

		public override void Update()
		{
			if (this.Running && !this.Finished)
			{
				this.action.Update();

				if (this.action.Finished)
				{
					if (this.times >= 0)
					{
						this.Count += 1;
					}
					this.action.Initialize(this.sprites);
				}
			}
		}

		public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
		{
			if (this.Running && !this.Finished)
			{
				this.action.Update(gameTime);

				if (this.action.Finished)
				{
					if (this.times >= 0)
					{
						this.Count += 1;
					}
					this.action.Initialize(this.sprites);
				}
			}
		}
	}
}