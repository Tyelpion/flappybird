﻿namespace GmCore.Actions
{
	using GmCore.Tweenings;
	using Microsoft.Xna.Framework;

	public class TransformAction : Action
	{
		private Tweening tweening;
		private float from;
		private float to;
		private float duration;
		private int frames;

		public TransformAction(float from, float to, int frames, Tweening tweening, params Sprite[] sprites)
			: base(sprites)
		{
			this.Init(from, to, frames, tweening);
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="duration">Seconds</param>
		/// <param name="tweening"></param>
		/// <param name="sprites"></param>
		public TransformAction(float from, float to, float duration, Tweening tweening, params Sprite[] sprites)
			: base(sprites)
		{
			this.Init(from, to, duration, tweening);
		}

		public override bool Finished
		{
			get
			{
				if (this.duration >= 0f)
				{
					return this.ElapsedTime >= this.duration;
				}
				else if (this.frames >= 0)
				{
					return this.Count == this.frames;
				}
				else
				{
					return false;
				}
			}
		}

		protected float From
		{
			get { return this.from; }
		}

		protected float To
		{
			get { return this.to; }
		}

		protected float Duration
		{
			get { return this.duration; }
		}

		protected int Frames
		{
			get { return this.frames; }
		}

		protected Tweening Tweening
		{
			get { return this.tweening; }
		}

		public override void Update()
		{
			if (this.frames >= 0 && this.Running && !this.Finished)
			{
				this.Count += 1;
			}
		}

		public override void Update(GameTime gameTime)
		{
			if (this.duration >= 0f && this.Running && !this.Finished)
			{
				this.ElapsedTime += (float) gameTime.ElapsedGameTime.TotalSeconds;
			}
		}

		protected float Update(float time, float begin, float change, int frames)
		{
			if (this.frames >= 0)
			{
				this.tweening.Update(time, begin, change, frames);
				return this.tweening.Value;
			}

			return 0f;
		}

		protected float Update(float time, float begin, float change, int frames, TweeningFunction function)
		{
			if (this.frames >= 0)
			{
				return this.tweening.Update(time, begin, change, frames, function);
			}

			return 0f;
		}

		protected float Update(float time, float begin, float change, float duration)
		{
			if (this.duration >= 0f)
			{
				this.tweening.Update(time, begin, change, duration);
				return this.tweening.Value;
			}

			return 0f;
		}

		protected float Update(float time, float begin, float change, float duration, TweeningFunction function)
		{
			if (this.duration >= 0f)
			{
				return this.tweening.Update(time, begin, change, duration, function);
			}

			return 0f;
		}

		private void Init(float from, float to, int frames, Tweening tweening)
		{
			this.Init(from, to, tweening);
			this.frames = frames;
			this.duration = -1f;
		}

		private void Init(float from, float to, float duration, Tweening tweening)
		{
			this.Init(from, to, tweening);
			this.duration = duration;
			this.frames = -1;
		}

		private void Init(float from, float to, Tweening tweening)
		{
			if (tweening != null)
			{
				this.from = from;
				this.to = to;
				this.tweening = tweening;
			}
			else
			{
				throw new System.ArgumentNullException("tweening");
			}
		}
	}
}