﻿namespace GmCore.Actions
{
	using GmCore.Tweenings;
	using Microsoft.Xna.Framework;

	public class ColorChangeAction : SimultaneousAction
	{
		public ColorChangeAction(Color from, Color to, int frames, Tweening tweening, params Sprite[] sprites)
			: this(from, to, frames, tweening, tweening, tweening, sprites)
		{
		}

		public ColorChangeAction(Color from, Color to, int frames, Tweening redTweening, Tweening greenTweening, Tweening blueTweening, params Sprite[] sprites)
			: base(
			new Action[]
			{
				new RedChangeAction(from.R, to.R, frames, redTweening),
				new GreenChangeAction(from.G, to.G, frames, greenTweening),
				new BlueChangeAction(from.B, to.B, frames, blueTweening)
			},
			sprites)
		{
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="duration">Seconds</param>
		/// <param name="tweening"></param>
		/// <param name="sprites"></param>
		public ColorChangeAction(Color from, Color to, float duration, Tweening tweening, params Sprite[] sprites)
			: this(from, to, duration, tweening, tweening, tweening, sprites)
		{
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="duration">Seconds</param>
		/// <param name="opacityTweening"></param>
		/// <param name="redTweening"></param>
		/// <param name="greenTweening"></param>
		/// <param name="blueTweening"></param>
		/// <param name="sprites"></param>
		public ColorChangeAction(Color from, Color to, float duration, Tweening redTweening, Tweening greenTweening, Tweening blueTweening, params Sprite[] sprites)
			: base(
			new Action[]
			{
				new RedChangeAction(from.R, to.R, duration, redTweening),
				new GreenChangeAction(from.G, to.G, duration, greenTweening),
				new BlueChangeAction(from.B, to.B, duration, blueTweening)
			},
			sprites)
		{
		}
	}
}