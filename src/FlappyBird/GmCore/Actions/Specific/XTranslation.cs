﻿namespace GmCore.Actions
{
	using GmCore.Tweenings;
	using Microsoft.Xna.Framework;

	public class XTranslationAction : TransformAction
	{
		public XTranslationAction(float from, float to, int frames, Tweening tweening, params Sprite[] sprites)
			: base(from, to, frames, tweening, sprites)
		{
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="duration">Seconds</param>
		/// <param name="tweening"></param>
		/// <param name="sprites"></param>
		public XTranslationAction(float from, float to, float duration, Tweening tweening, params Sprite[] sprites)
			: base(from, to, duration, tweening, sprites)
		{
		}

		public override void Initialize(params Sprite[] sprites)
		{
			base.Initialize(sprites);
			foreach (var sprite in this.sprites)
			{
				sprite.GoTo(this.From, null);
			}
		}

		public override void Update()
		{
			base.Update();
			if (this.Running)
			{
				if (!this.Finished)
				{
					foreach (var sprite in this.sprites)
					{
						sprite.GoTo(this.Update(this.Count, this.From, this.To - this.From, this.Frames), null);
					}
				}
				else
				{
					this.DoFinalWork();
				}
			}
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			if (this.Running)
			{
				if (!this.Finished)
				{
					foreach (var sprite in this.sprites)
					{
						sprite.GoTo(this.Update(this.ElapsedTime, this.From, this.To - this.From, this.Duration), null);
					}
				}
				else
				{
					this.DoFinalWork();
				}
			}
		}

		protected override void DoFinalWork()
		{
			foreach (var sprite in this.sprites)
			{
				sprite.GoTo(this.To, null);
			}

			base.DoFinalWork();
		}
	}
}