﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	public class NumberSprite : Sprite
	{
		private struct Number
		{
			public int Value;
			public Vector2 Position;
		}

		private int value;
		private int space;
		private int prevLen;
		private Number[] numbers;
		private Rectangle region;
		private TextureRegion textureRegion;

		public NumberSprite(TextureRegion textureRegion, int? value = null)
			: this(Vector2.Zero, Alignment.TopLeft, textureRegion, value)
		{
		}

		public NumberSprite(Vector2 position, TextureRegion textureRegion, int? value = null)
			: this(position, Alignment.TopLeft, textureRegion, Color.White, value)
		{
		}

		public NumberSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, int? value = null)
			: this(position, originPosition, textureRegion, Color.White, value)
		{
		}

		public NumberSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, Color color, int? value = null)
			: this(position, originPosition, textureRegion, 0f, color, value)
		{
		}

		public NumberSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, float angle, Color color, int? value = null)
			: this(position, originPosition, textureRegion, angle, color, 1f, value)
		{
		}

		public NumberSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, float angle, Color color, float scale, int? value = null)
			: base(position, originPosition, angle, color, scale)
		{
			if (textureRegion != null)
			{
				if (textureRegion.Frames.Length == 10)
				{
					this.textureRegion = textureRegion;
					this.space = this.textureRegion.Width + this.textureRegion.Width / 10;
					this.value = (value == null) ? 0 : value.Value;
					this.prevLen = 0;
					this.region = new Rectangle(0, 0, 0, this.textureRegion.Height);
					this.UpdateNumbers();
				}
				else
				{
					throw new System.ArgumentException("Not enough frames.", "textureRegion");
				}
			}
			else
			{
				throw new System.ArgumentNullException("textureRegion");
			}
		}

		public int Value
		{
			get
			{
				return this.value;
			}

			set
			{
				if (this.value != value)
				{
					this.value = value;
					this.UpdateNumbers();
				}
			}
		}

		public override int Width
		{
			get
			{
				if (this.numbers != null && this.numbers.Length > 0)
				{
					return this.region.Width;
				}

				return 0;
			}
		}

		public override int Height
		{
			get { return this.region.Height; }
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (this.numbers != null && this.numbers.Length > 0)
			{
				this.UpdatePositions(this.Position);
				
				for (var i = 0; i < this.numbers.Length; ++i)
				{
					spriteBatch.Draw(this.textureRegion.Texture, this.numbers[i].Position, this.textureRegion.Frames[this.numbers[i].Value], this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);
				}

				base.Draw(spriteBatch);
			}
		}

		private void UpdateNumbers()
		{
			var data = this.value.ToString();
			this.numbers = new Number[data.Length];

			for (var i = 0; i < data.Length; ++i)
			{
				this.numbers[i].Value = int.Parse(data[i].ToString());
				this.numbers[i].Position = new Vector2(this.X + (this.space) * i, this.Y);
			}

			if (this.prevLen != data.Length)
			{
				this.region.Width = this.space * this.numbers.Length - (this.space - this.textureRegion.Width);
				this.prevLen = data.Length;
			}
		}

		private void UpdatePositions(Vector2 value)
		{
			if (this.numbers != null)
			{
				for (var i = 0; i < this.numbers.Length; ++i)
				{
					this.numbers[i].Position.X = value.X + this.space * i;
					this.numbers[i].Position.Y = value.Y;
				}

				this.SetBoundary(this.Width, this.Height);
				this.SetOrigin(this.Boundary);
			}
		}
	}
}