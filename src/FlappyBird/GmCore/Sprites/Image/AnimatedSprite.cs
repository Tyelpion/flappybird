﻿namespace GmCore
{
	using Microsoft.Xna.Framework;

	public class AnimatedSprite : ImageSprite
	{
		private int elapsedTime;
		private int frameTime;
		protected int resetFrame;

		public AnimatedSprite(Vector2 position, TextureRegion textureRegion, int frameTime, bool loop)
			: this(position, Alignment.TopLeft, textureRegion, frameTime, loop)
		{
		}

		public AnimatedSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, int frameTime, bool loop)
			: this(position, originPosition, textureRegion, frameTime, loop, Color.White)
		{
		}

		public AnimatedSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, int frameTime, bool loop, Color color)
			: base(position, originPosition, textureRegion, color)
		{

			this.elapsedTime = 0;
			this.Active = true;
			this.frameTime = frameTime;
			this.Loop = loop;
		}

		public bool Active
		{
			get;
			protected set;
		}

		public bool Loop
		{
			get;
			protected set;
		}

		public override void Update(GameTime gameTime)
		{
			if (this.Active)
			{
				this.elapsedTime += gameTime.ElapsedGameTime.Milliseconds;

				if (this.elapsedTime > this.frameTime)
				{
					this.textureRegion.MoveNext(this.Loop);

					if (this.textureRegion.IsLast && !Loop)
					{
						this.Active = false;
					}

					this.elapsedTime = 0;
				}
			}

			base.Update(gameTime);
		}
	}
}