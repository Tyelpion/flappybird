﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	public class ImageSprite : Sprite
	{
		protected TextureRegion textureRegion;

		public ImageSprite(TextureRegion textureRegion)
			: this(Vector2.Zero, Alignment.TopLeft, textureRegion)
		{
		}

		public ImageSprite(Vector2 position, TextureRegion textureRegion)
			: this(position, Alignment.TopLeft, textureRegion, Color.White)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion)
			: this(position, originPosition, textureRegion, Color.White)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, Color color)
			: this(position, originPosition, textureRegion, 0f, color)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, float angle, Color color)
			: this(position, originPosition, textureRegion, angle, color, 1f)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, float angle, Color color, float scale)
			: base(position, originPosition, angle, color, scale)
		{
			this.ChangeTextureRegion(textureRegion);
		}

		public TextureRegion TextureRegion
		{
			get { return this.textureRegion; }
		}

		public override int Width
		{
			get { return this.textureRegion.Width; }
		}

		public override int Height
		{
			get { return this.textureRegion.Height; }
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Begin();
			spriteBatch.Draw(this.textureRegion.Texture, this.Position, this.textureRegion.Region, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);
			spriteBatch.End();
			base.Draw(spriteBatch);
		}

		public void ChangeTextureRegion(TextureRegion textureRegion)
		{
			if (textureRegion != null)
			{
				this.textureRegion = textureRegion;
				this.SetBoundary(this.textureRegion.Region);
				this.SetOrigin(this.textureRegion.Region);
			}
			else
			{
				throw new System.ArgumentNullException("textureRegion");
			}
		}
	}
}