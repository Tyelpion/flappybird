﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using System.Collections.Generic;

	public class Sprite
	{
		private Sprite parent;
		private Color color;
		private Vector2 position;
		private Vector2 originalPosition;
		private Vector2 origin;
		private Vector2 offset;
		private Rectangle boundary;
		private Alignment originPosition;
		private LinkedList<Sprite> children;

		public Sprite()
			: this(Vector2.Zero)
		{
		}

		public Sprite(Vector2 position)
			: this(position, Alignment.TopLeft)
		{
		}

		public Sprite(Vector2 position, Alignment originPosition)
			: this(position, originPosition, Color.White)
		{
		}

		public Sprite(Vector2 position, Alignment originPosition, Color color)
			: this(position, originPosition, 0f, Color.White, 1f)
		{
		}

		public Sprite(Vector2 position, Alignment originPosition, float angle, Color color, float scale)
		{
			this.Angle = angle;
			this.Scale = scale;
			this.color = color;
			this.originPosition = originPosition;
			this.boundary = new Rectangle((int) position.X, (int) position.Y, 0, 0);
			this.originalPosition = position;
			this.X = position.X;
			this.Y = position.Y;
			this.children = new LinkedList<Sprite>();
		}

		public Sprite Parent
		{
			get { return this.parent; }
		}

		public LinkedList<Sprite> Children
		{
			get { return this.children; }
		}

		public float Scale { get; set; }

		public float Angle { get; set; }

		public Vector2 Offset
		{
			get
			{
				return this.offset;
			}

			protected set
			{
				this.offset = value;
			}
		}

		public Vector2 Origin
		{
			get
			{
				return this.origin;
			}

			protected set
			{
				this.origin = value;
			}
		}

		public Vector2 Position
		{
			get
			{
				return this.position;
			}
		}

		public float X
		{
			get
			{
				return this.position.X;
			}

			protected set
			{
				this.position.X = value;
				this.UpdateX();
			}
		}

		public float Y
		{
			get
			{
				return this.position.Y;
			}

			protected set
			{
				this.position.Y = value;
				this.UpdateY();
			}
		}

		public Color Color
		{
			get
			{
				return this.color;
			}

			protected set
			{
				this.color = value;
			}
		}

		public float Opacity
		{
			get
			{
				return this.color.A / 255f;
			}

			set
			{
				this.color.A = (byte) (value * 255);
			}
		}

		public float Red
		{
			get
			{
				return this.color.R / 255f;
			}

			set
			{
				this.color.R = (byte) (value * 255);
			}
		}

		public float Green
		{
			get
			{
				return this.color.G / 255f;
			}

			set
			{
				this.color.G = (byte) (value * 255);
			}
		}

		public float Blue
		{
			get
			{
				return this.color.B / 255f;
			}

			set
			{
				this.color.B = (byte) (value * 255);
			}
		}

		public Rectangle Boundary
		{
			get
			{
				return this.boundary;
			}

			protected set
			{
				this.boundary = value;
			}
		}

		public virtual int Width
		{
			get { return this.Boundary.Width; }
		}

		public virtual int Height
		{
			get { return this.Boundary.Height; }
		}

		public Alignment OriginPosition
		{
			get { return this.originPosition; }
		}

		public virtual void Update(GameTime gameTime)
		{
			foreach (var child in this.children)
			{
				child.Update(gameTime);
			}
		}

		public virtual void Draw(SpriteBatch spriteBatch)
		{
			foreach (var child in this.children)
			{
				child.Draw(spriteBatch);
			}
		}

		public virtual void Move(Vector2 amount)
		{
			this.Move(amount.X, amount.Y);
		}

		public virtual void Move(float x, float y)
		{
			this.X += x;
			this.Y += y;
		}

		public virtual void MoveLeft(float amount)
		{
			this.X -= amount;
		}

		public virtual void MoveRight(float amount)
		{
			this.X += amount;
		}

		public virtual void MoveUp(float amount)
		{
			this.Y -= amount;
		}

		public virtual void MoveDown(float amount)
		{
			this.Y += amount;
		}

		public virtual void GoTo(Vector2 position)
		{
			this.GoTo(position.X, position.Y);
		}

		public virtual void GoTo(float x, float y)
		{
			this.X = x;
			this.Y = y;
		}

		public virtual void GoTo(float? x, float? y)
		{
			if (x != null)
			{
				this.X = x.Value;
			}

			if (y != null)
			{
				this.Y = y.Value;
			}
		}

		public virtual bool Intersects(Sprite sprite)
		{
			return this.Boundary.Intersects(sprite.Boundary);
		}

		public void AddChild(Sprite child)
		{
			if (child == null)
				throw new System.ArgumentNullException("child");

			if (!this.Children.Contains(child))
			{
				this.Adding(child);
			}
		}

		public void AddChild(params Sprite[] children)
		{
			if (children == null)
				throw new System.ArgumentNullException("children");

			foreach (var child in children)
			{
				if (!this.Children.Contains(child))
				{
					this.Adding(child);
				}
			}
		}

		public void RemoveChild(Sprite child)
		{
			if (child == null)
				throw new System.ArgumentNullException("child");

			if (this.Children.Contains(child))
			{
				this.Removing(child);
			}

		}

		public void RemoveChild(params Sprite[] children)
		{
			if (children == null)
				throw new System.ArgumentNullException("children");

			foreach (var child in children)
			{
				if (this.Children.Contains(child))
				{
					this.Removing(child);
				}
			}
		}

		protected void UpdateChildrenPosition()
		{
			if (this.children != null)
			{
				foreach (var child in children)
				{
					child.IncludeParentOffset();
				}
			}
		}

		protected void IncludeParentOffset()
		{
			if (this.parent != null)
			{
				this.X = this.originalPosition.X + this.parent.offset.X;
				this.Y = this.originalPosition.Y + this.parent.offset.Y;
			}
		}

		protected void ExcludeParentOffset()
		{
			if (this.parent != null)
			{
				this.X = this.originalPosition.X;
				this.Y = this.originalPosition.Y;
			}
		}

		protected virtual void SetBoundary(Rectangle region)
		{
			this.SetBoundary(region.Width, region.Height);
		}

		protected virtual void SetBoundary(int width, int height)
		{
			this.boundary.X = (int) this.offset.X;
			this.boundary.Y = (int) this.offset.Y;
			this.boundary.Width = width;
			this.boundary.Height = height;
		}

		protected virtual void UpdateX()
		{
			this.SetOriginX(this.boundary.Width);
			this.boundary.X = (int) this.offset.X;
			this.UpdateChildrenPosition();
		}

		protected virtual void UpdateY()
		{
			this.SetOriginY(this.boundary.Height);
			this.boundary.Y = (int) this.offset.Y;
			this.UpdateChildrenPosition();
		}

		protected void SetOrigin(Rectangle region)
		{
			this.SetOrigin(region.Width, region.Height);
		}

		protected void SetOriginX(Rectangle region)
		{
			this.SetOriginX(region.Width);
		}

		protected void SetOriginY(Rectangle region)
		{
			this.SetOriginY(region.Height);
		}

		protected void SetOrigin(int width, int height)
		{
			this.SetOriginX(width);
			this.SetOriginY(height);
		}

		protected void SetOriginX(int width)
		{
			switch ((Alignment) ((int) this.originPosition & 0x0f))
			{
				case Alignment.HorizontalLeft:
					this.origin.X = 0;
					this.offset.X = this.position.X;
					break;

				case Alignment.HorizontalCenter:
					this.origin.X = width / 2f;
					this.offset.X = this.position.X - this.origin.X;
					break;

				case Alignment.HorizontalRight:
					this.origin.X = width;
					this.offset.X = this.position.X - this.origin.X;
					break;

				default:
					break;
			}
		}

		protected void SetOriginY(int height)
		{
			switch ((Alignment) ((int) this.originPosition & 0xf0))
			{
				case Alignment.VerticalTop:
					this.origin.Y = 0;
					this.offset.Y = this.position.Y;
					break;

				case Alignment.VerticalMiddle:
					this.origin.Y = height / 2f;
					this.offset.Y = this.position.Y - this.origin.Y;
					break;

				case Alignment.VerticalBottom:
					this.origin.Y = height;
					this.offset.Y = this.position.Y - this.origin.Y;
					break;

				default:
					break;
			}
		}

		private void Adding(Sprite child)
		{
			child.parent = this;
			child.IncludeParentOffset();
			this.Children.AddLast(child);
		}

		private void Removing(Sprite child)
		{
			this.Children.Remove(child);
			child.ExcludeParentOffset();
			child.parent = null;
		}
	}
}