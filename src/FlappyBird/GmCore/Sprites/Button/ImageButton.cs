﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using Microsoft.Xna.Framework.Input;

	public class ImageButton : Button
	{
		private TextureRegion upImage;
		private TextureRegion downImage;
		private TextureRegion disabledImage;
		private TextureRegion currentImage;
		protected MouseState previousMouse;

		public ImageButton(Screen parent, Vector2 position, TextureRegion upImage, TextureRegion downImage = null, TextureRegion disabledImage = null)
			: this(parent, position, Alignment.TopLeft, upImage, downImage, disabledImage, Color.White)
		{
		}

		public ImageButton(Screen parent, Vector2 position, Alignment originPosition, TextureRegion upImage, TextureRegion downImage = null, TextureRegion disabledImage = null)
			: this(parent, position, originPosition, upImage, downImage, disabledImage, Color.White)
		{
		}

		public ImageButton(Screen parent, Vector2 position, Alignment originPosition, TextureRegion upImage, TextureRegion downImage, TextureRegion disabledImage, Color color)
			: base(parent, position, originPosition, color)
		{
			if (upImage != null)
			{
				this.upImage = upImage;
				this.currentImage = upImage;
				this.Enabled = true;

				if (downImage != null)
				{
					this.downImage = downImage;
				}
				else
				{
					this.downImage = upImage;
				}

				if (disabledImage != null)
				{
					this.disabledImage = disabledImage;
				}
				else
				{
					this.disabledImage = upImage;
				}
				this.SetBoundary(this.currentImage.Region);
				this.SetOrigin(this.currentImage.Region);
			}
			else
			{
				throw new System.ArgumentNullException("upImage");
			}
		}

		public override int Width
		{
			get { return this.currentImage.Width; }
		}

		public override int Height
		{
			get { return this.currentImage.Height; }
		}

		public TextureRegion UpImage
		{
			get { return this.upImage; }
		}

		public TextureRegion DownImage
		{
			get { return this.downImage; }
		}

		public TextureRegion DisabledImage
		{
			get { return this.disabledImage; }
		}

		public bool Enabled { get; set; }

		public bool IsFocused { get; set; }

		public override void OnStateChanged()
		{
			switch (this.state)
			{
				case GmButtonState.Disable:
					this.currentImage = this.disabledImage;
					break;

				case GmButtonState.Down:
					this.currentImage = this.downImage;
					break;

				case GmButtonState.Up:
					this.currentImage = this.upImage;
					break;
			}
		}

		public override void UpdateVisualState()
		{
			if (this.state != GmButtonState.Disable)
			{
				var mouseState = Mouse.GetState();

				if (mouseState.LeftButton == ButtonState.Pressed)
				{
					if (this.IsFocused)
					{
						if (this.Boundary.Contains(mouseState.X, mouseState.Y))
						{
							this.currentImage = this.downImage;
						}
					}
					else
					{
						if (this.previousMouse.LeftButton == ButtonState.Released)
						{
							if (this.Boundary.Contains(mouseState.X, mouseState.Y))
							{
								this.IsFocused = true;
							}
						}
					}
				}
				else if (mouseState.LeftButton == ButtonState.Released)
				{
					if (this.IsFocused)
					{
						if (this.Boundary.Contains(mouseState.X, mouseState.Y))
						{
							this.OnClicked();
						}

						this.IsFocused = false;
					}

					this.currentImage = this.upImage;
				}

				this.previousMouse = mouseState;
			}
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Begin();
			spriteBatch.Draw(this.currentImage.Texture, this.Position, this.currentImage.Region, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);
			spriteBatch.End();
			base.Draw(spriteBatch);
		}
	}
}