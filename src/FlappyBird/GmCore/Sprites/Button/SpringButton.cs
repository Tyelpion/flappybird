﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Input;

	public class SpringButton : ImageButton
	{
		private float spring;
		private float preserved;

		public SpringButton(Screen parent, Vector2 position, TextureRegion upImage)
			: this(parent, position, Alignment.TopLeft, upImage, null, Color.White)
		{
		}

		public SpringButton(Screen parent, Vector2 position, TextureRegion upImage, Color color)
			: this(parent, position, Alignment.TopLeft, upImage, null, color)
		{
		}

		public SpringButton(Screen parent, Vector2 position, TextureRegion upImage, TextureRegion disableImage)
			: this(parent, position, Alignment.TopLeft, upImage, disableImage, Color.White)
		{
		}

		public SpringButton(Screen parent, Vector2 position, Alignment originPosition, TextureRegion upImage)
			: this(parent, position, originPosition, upImage, null, Color.White)
		{
		}

		public SpringButton(Screen parent, Vector2 position, Alignment originPosition, TextureRegion upImage, Color color)
			: this(parent, position, originPosition, upImage, null, color)
		{
		}

		public SpringButton(Screen parent, Vector2 position, Alignment originPosition, TextureRegion upImage, TextureRegion disableImage)
			: this(parent, position, originPosition, upImage, disableImage, Color.White)
		{
		}

		public SpringButton(Screen parent, Vector2 position, Alignment originPosition, TextureRegion upImage, TextureRegion disableImage, Color color)
			: base(parent, position, originPosition, upImage, null, disableImage, color)
		{
			this.preserved = this.Y;
			this.spring = this.Y + 4f;
		}

		public override void UpdateVisualState()
		{
			if (this.state != GmButtonState.Disable)
			{
				var mouse = Mouse.GetState();

				if (mouse.LeftButton == ButtonState.Pressed)
				{
					if (this.IsFocused)
					{
						if (this.Boundary.Contains(mouse.X, mouse.Y))
						{
							this.Y = this.spring;
						}
					}
					else
					{
						if (this.previousMouse.LeftButton == ButtonState.Released)
						{
							if (this.Boundary.Contains(mouse.X, mouse.Y))
							{
								this.IsFocused = true;
							}
						}
					}
				}
				else if (mouse.LeftButton == ButtonState.Released)
				{
					if (this.IsFocused)
					{
						if (this.Boundary.Contains(mouse.X, mouse.Y))
						{
							this.OnClicked();
						}

						this.IsFocused = false;
					}

					this.Y = this.preserved;
				}
			}
		}
	}
}