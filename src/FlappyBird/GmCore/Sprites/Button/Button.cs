﻿namespace GmCore
{
	using Microsoft.Xna.Framework;

	public abstract class Button : Sprite, IButton
	{
		protected Screen screenParent;
		protected GmButtonState state;

		public Button(Screen parent, Vector2 position)
			: this(parent, position, Alignment.TopLeft)
		{
		}

		public Button(Screen parent, Vector2 position, Alignment originPosition)
			: this(parent, position, originPosition, Color.White)
		{
		}

		public Button(Screen parent, Vector2 position, Alignment originPosition, Color color)
			: base(position, originPosition, color)
		{
			if (parent != null)
			{
				this.screenParent = parent;
				this.screenParent.AddButton(this);
			}
			else
			{
				throw new System.ArgumentNullException("parent");
			}
		}

		public event HandlerClick Click;

		public Screen ScreenParent
		{
			get { return this.screenParent; }
		}

		public GmButtonState State
		{
			get
			{
				return this.state;
			}

			set
			{
				if (this.state != value)
				{
					this.state = value;
					this.OnStateChanged();
				}
			}
		}

		public abstract void OnStateChanged();

		public abstract void UpdateVisualState();

		public virtual void OnClicked()
		{
			if (this.state != GmButtonState.Disable && this.Click != null)
			{
				this.Click(new ButtonEventsArgs(this));
			}
		}
	}
}