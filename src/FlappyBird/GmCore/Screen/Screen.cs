﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using Microsoft.Xna.Framework.Input;
	using System.Collections.Generic;

	public abstract class Screen
	{
		private Sprite root;
		private List<IButton> buttons;
		private Rectangle boundary;

		protected Screen()
			: this(0, 0)
		{
		}

		protected Screen(int width, int height)
		{
			this.root = new Sprite();
			this.buttons = new List<IButton>();
			this.boundary = new Rectangle(0, 0, width, height);
		}

		public Sprite Root
		{
			get
			{
				return this.root;
			}
		}

		public int ScreenWidth
		{
			get
			{
				return this.boundary.Width;
			}
		}

		public int ScreenHeight
		{
			get
			{
				return this.boundary.Height;
			}
		}

		public abstract void Start();

		public abstract void End();

		public abstract void UpdateLogic(GameTime gameTime);

		public bool Contains(int x, int y)
		{
			return this.boundary.Contains(x, y);
		}

		public bool Contains(Point value)
		{
			return this.boundary.Contains(value);
		}

		public bool Contains(Rectangle value)
		{
			return this.boundary.Contains(value);
		}

		public bool Contains(MouseState mouse)
		{
			return this.boundary.Contains(mouse.X, mouse.Y);
		}

		public void Update(GameTime gameTime)
		{
			this.UpdateButton();
			this.UpdateLogic(gameTime);
		}

		public void AddChild(Sprite sprite)
		{
			if (sprite != null)
			{
				this.root.AddChild(sprite);
			}
			else
			{
				throw new System.ArgumentNullException("sprite");
			}
		}

		public void AddButton(IButton button)
		{
			if (button != null)
			{
				if (!this.buttons.Contains(button))
				{
					this.buttons.Add(button);
				}
			}
			else
			{
				throw new System.ArgumentNullException("button");
			}
		}

		public void RemoveChild(Sprite sprite)
		{
			if (sprite != null)
			{
				this.root.RemoveChild(sprite);
			}
			else
			{
				throw new System.ArgumentNullException("sprite");
			}
		}

		public void RemoveButton(Button button)
		{
			if (button != null)
			{
				if (!this.buttons.Contains(button))
				{
					this.buttons.Remove(button);
				}
			}
			else
			{
				throw new System.ArgumentNullException("button");
			}
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			this.root.Draw(spriteBatch);
		}

		private void UpdateButton()
		{
			for (var i = 0; i < buttons.Count; ++i)
			{
				this.buttons[i].UpdateVisualState();
			}
		}
	}
}