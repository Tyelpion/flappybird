﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Audio;
	using Microsoft.Xna.Framework.Content;
	using Microsoft.Xna.Framework.Graphics;
	using Microsoft.Xna.Framework.Media;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Xml.Linq;
	using System.Windows.Forms;

	public static class ResourceManager
	{
		private static int screenWidth = 800;
		private static int screenHeight = 600;
		private static int frameTime = 0;
		private static int highestScore = 0;
		private static Form gameWindow;
		private static Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();
		private static Dictionary<string, TextureRegion> textureRegions = new Dictionary<string, TextureRegion>();
		private static Dictionary<string, SoundEffect> sounds = new Dictionary<string, SoundEffect>();
		private static Dictionary<string, Song> songs = new Dictionary<string, Song>();
		public static Random Random = new Random();

		public static int ScreenWidth
		{
			get { return screenWidth; }
		}

		public static int ScreenHeight
		{
			get { return screenHeight; }
		}

		public static int HighestScore
		{
			get { return highestScore; }
		}

		public static int FrameTime
		{
			get
			{
				return frameTime;
			}

			set
			{
				frameTime = value;
			}
		}

		public static void SetGameWindow(GameWindow window)
		{
			gameWindow = Control.FromHandle(window.Handle) as Form;
			screenWidth = window.ClientBounds.Width;
			screenHeight = window.ClientBounds.Height;
		}

		public static bool IsGameActive()
		{
			return Form.ActiveForm == gameWindow;
		}

		public static void SetHighestScore(int score)
		{
			if (highestScore < score)
			{
				highestScore = score;
			}
		}

		public static void AddTexture(string name, Texture2D texture)
		{
			if (!textures.ContainsKey(name) && texture != null)
			{
				textures.Add(name, texture);
			}
		}

		public static void AddTextureRegion(string name, TextureRegion textureRegion)
		{
			if (!textureRegions.ContainsKey(name) && textureRegion != null)
			{
				textureRegions.Add(name, textureRegion);
			}
		}

		public static void AddSound(string name, SoundEffect sound)
		{
			if (!sounds.ContainsKey(name) && sound != null)
			{
				sounds.Add(name, sound);
			}
		}

		public static void AddSong(string name, Song song)
		{
			if (!songs.ContainsKey(name) && song != null)
			{
				songs.Add(name, song);
			}
		}

		public static Texture2D GetTexture(string name)
		{
			return textures.ContainsKey(name) ? textures[name] : null;
		}

		public static TextureRegion GetTextureRegion(string name)
		{
			return textureRegions.ContainsKey(name) ? textureRegions[name] : null;
		}

		public static SoundEffect GetSound(string name)
		{
			return sounds.ContainsKey(name) ? sounds[name] : null;
		}

		public static Song GetSong(string name)
		{
			return songs.ContainsKey(name) ? songs[name] : null;
		}

		public static void RemoveTexture(string name)
		{
			if (textures.ContainsKey(name))
			{
				textures.Remove(name);
			}
		}

		public static void RemoveTextureRegion(string name)
		{
			if (textureRegions.ContainsKey(name))
			{
				textureRegions.Remove(name);
			}
		}

		public static void RemoveSound(string name)
		{
			if (sounds.ContainsKey(name))
			{
				sounds.Remove(name);
			}
		}

		public static void RemoveSong(string name)
		{
			if (songs.ContainsKey(name))
			{
				songs.Remove(name);
			}
		}

		public static void LoadResources(ContentManager contentManager, string path)
		{
			if (File.Exists(path))
			{
				var doc = XDocument.Load(path);

				LoadTextures(doc, contentManager);
				LoadSounds(doc, contentManager);
				LoadSongs(doc, contentManager);
			}
		}

		private static Point ToPoint(string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				var arr = value.Split();
				if (arr.Length == 2)
				{
					return new Point(Convert(arr[0]), Convert(arr[1]));
				}

				return Point.Zero;
			}

			return Point.Zero;
		}

		private static void Convert(string value, out int result)
		{
			if (!int.TryParse(value, out result))
			{
				result = 0;
			}
		}

		private static int Convert(string value)
		{
			int result;
			if (!int.TryParse(value, out result))
			{
				result = 0;
			}

			return result;
		}

		private static bool IsValidDirection(string value)
		{
			return !string.IsNullOrEmpty(value) && (value.Equals("vertical") || value.Equals("horizontal"));
		}

		private static void LoadSounds(XDocument doc, ContentManager contentManager)
		{
			if (doc != null)
			{
				var sounds = doc.Root.Elements("Sounds");
				if (sounds != null)
				{
					foreach (var sound in doc.Descendants("Sound"))
					{
						if (sound.Attribute("name") != null && sound.Attribute("source") != null)
						{
							var content = contentManager.Load<SoundEffect>(sound.Attribute("source").Value);
							AddSound(sound.Attribute("name").Value, content);
						}
					}
				}
			}
		}

		private static void LoadSongs(XDocument doc, ContentManager contentManager)
		{
			if (doc != null)
			{
				var songs = doc.Root.Elements("Songs");
				if (songs != null)
				{
					foreach (var song in doc.Descendants("Song"))
					{
						if (song.Attribute("name") != null && song.Attribute("source") != null)
						{
							var content = contentManager.Load<Song>(song.Attribute("source").Value);
							AddSong(song.Attribute("name").Value, content);
						}
					}
				}
			}
		}

		private static void LoadTextures(XDocument doc, ContentManager contentManager)
		{
			if (doc != null)
			{
				var textures = doc.Root.Element("Textures");
				if (textures != null)
				{
					foreach (var texture in textures.Elements("Texture"))
					{
						var textureSource = texture.Attribute("source");
						var textureName = texture.Attribute("name");
						if (textureSource != null &&
							textureName != null &&
							!string.IsNullOrEmpty(textureSource.Value) &&
							!string.IsNullOrEmpty(textureName.Value))
						{
							var content = contentManager.Load<Texture2D>(textureSource.Value);
							AddTexture(textureName.Value, content);

							if (texture.Element("Region") == null)
							{
								AddTextureRegion(textureName.Value, new TextureRegion(content, new Rectangle(0, 0, content.Width, content.Height)));
							}
							else
							{
								foreach (var region in texture.Descendants("Region"))
								{
									var regionName = region.Attribute("name");
									if (regionName != null && !string.IsNullOrEmpty(regionName.Value))
									{
										if (region.Attribute("lefttop") != null)
										{
											var lefttop = ToPoint(region.Attribute("lefttop").Value);
											if (region.Attribute("rightbottom") != null ||
												(region.Attribute("width") != null && region.Attribute("height") != null))
											{
												Rectangle firstRegion;
												if (region.Attribute("rightbottom") != null)
												{
													var rightbottom = ToPoint(region.Attribute("rightbottom").Value);
													firstRegion = new Rectangle(lefttop.X, lefttop.Y, rightbottom.X - lefttop.X, rightbottom.Y - lefttop.Y);
												}
												else
												{
													var width = Convert(region.Attribute("width").Value);
													var height = Convert(region.Attribute("height").Value);
													firstRegion = new Rectangle(lefttop.X, lefttop.Y, width, height);
												}

												if (region.Attribute("totalFrames") != null)
												{
													var totalFrames = Convert(region.Attribute("totalFrames").Value);

													if (totalFrames > 1)
													{
														if (region.Attribute("direction") != null)
														{
															var direction = region.Attribute("direction").Value;
															if (IsValidDirection(direction) &&
																region.Attribute("maxFramesInLine") != null &&
																region.Attribute("frameStep") != null &&
																region.Attribute("lineStep") != null)
															{
																var maxFramesInLine = Convert(region.Attribute("maxFramesInLine").Value);
																if (totalFrames >= maxFramesInLine)
																{
																	var frameStep = Convert(region.Attribute("frameStep").Value);
																	var lineStep = Convert(region.Attribute("lineStep").Value);
																	var isHorizontal = direction.Equals("horizontal") ? true : false;

																	AddTextureRegion(regionName.Value,
																			new TextureRegion(content, firstRegion, totalFrames, maxFramesInLine, isHorizontal, frameStep, lineStep));
																}
															}
														}
														else if (region.Attribute("frameStep") != null)
														{
															var frameStep = ToPoint(region.Attribute("frameStep").Value);

															AddTextureRegion(regionName.Value,
																new TextureRegion(content, firstRegion, totalFrames, frameStep));
														}
													}
													else if (totalFrames > 0)
													{
														AddTextureRegion(regionName.Value, new TextureRegion(content, firstRegion));
													}
												}
												else
												{
													AddTextureRegion(regionName.Value, new TextureRegion(content, firstRegion));
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else
			{
				throw new System.ArgumentNullException("doc");
			}
		}
	}
}