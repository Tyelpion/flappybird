﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using GmCore;

	public static class ScreenManager
	{
		private static Screen thisCurrent = null;
		private static Screen thisNext = null;

		public static Screen Current
		{
			get { return thisCurrent; }
		}

		public static Screen Next
		{
			get { return thisNext; }
		}

		public static void Switch(Screen next)
		{
			thisNext = next;
		}

		public static void Update(GameTime gameTime)
		{
			if (thisCurrent != thisNext)
			{
				if (thisCurrent != null)
				{
					thisCurrent.End();
				}

				if (thisNext != null)
				{
					thisNext.Start();
				}

				thisCurrent = thisNext;
			}

			if (thisCurrent != null)
			{
				thisCurrent.Update(gameTime);
			}
		}

		public static void Draw(SpriteBatch spriteBatch)
		{
			if (thisCurrent != null)
			{
				thisCurrent.Draw(spriteBatch);
			}
		}
	}
}