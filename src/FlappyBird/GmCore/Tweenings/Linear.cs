﻿namespace GmCore.Tweenings
{
	public class LinearTweening : Tweening
	{
		public LinearTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			return change * time / duration + begin;
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			return change * time / duration + begin;
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			return change * time / duration + begin;
		}
	}
}