﻿namespace GmCore.Tweenings
{
	using System;

	public class CircularTweening : Tweening
	{
		public CircularTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			time = time / duration;

			return -change * ((float) Math.Sqrt(1f - time * time) - 1f) + begin;
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			time = time / duration - 1f;

			return change * (float) Math.Sqrt(1f - time * time) + begin;
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			time = time / duration / 2f;

			if (time < 1f)
			{
				return (-change / 2f) * ((float) Math.Sqrt(1f - time * time) - 1f) + begin;
			}

			time = time - 2f;

			return (change / 2f) * ((float) Math.Sqrt(1f - time * time) + 1f) + begin;
		}
	}
}