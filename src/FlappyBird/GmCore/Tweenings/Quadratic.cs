﻿namespace GmCore.Tweenings
{
	public class QuadraticTweening : Tweening
	{
		public QuadraticTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			time = time / duration;

			return change * time * time + begin;
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			time = time / duration;

			return -change * time * (time - 2f) + begin;
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			time = time / duration / 2f;

			if (time < 1f)
			{
				return (change / 2f) * time * time + begin;
			}

			time -= 1f;

			return (-change / 2f) * (time * (time - 2f) - 1f) + begin;
		}
	}
}