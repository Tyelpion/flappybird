﻿namespace GmCore.Tweenings
{
	public class QuarticTweening : Tweening
	{
		public QuarticTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			time = time / duration;

			return change * time * time * time * time + begin;
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			time = time / duration - 1f;

			return -change * (time * time * time * time - 1f) + begin;
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			time = time / duration / 2f;

			if (time < 1f)
			{
				return change / 2f * time * time * time * time + begin;
			}

			time = time - 2f;

			return (-change / 2f) * (time * time * time * time - 2f) + begin;
		}
	}
}