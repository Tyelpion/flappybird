﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	public class TextureRegion
	{
		private Texture2D texture;
		private Rectangle[] frames;
		private int currentFrame;
		//private Color[][] data;

		public TextureRegion(Texture2D texture, Rectangle firstRegion, int totalFrames, int maxFramesInLine, bool isHorizontal, int frameStep, int lineStep)
			: this(texture)
		{
			if (totalFrames > 0 && maxFramesInLine > 0)
			{
				if (totalFrames >= maxFramesInLine)
				{
					this.frames = new Rectangle[totalFrames];
					this.frames[0] = firstRegion;

					for (int i = 1, line = 0, offset = 0; i < totalFrames; ++i)
					{
						// Ranges from 0 to maxFramesInLine
						var next = i - offset;

						this.frames[i] = new Rectangle(
							firstRegion.X + (isHorizontal ? (frameStep * next) : (lineStep * line)),
							firstRegion.Y + (isHorizontal ? (lineStep * line) : (frameStep * next)),
							firstRegion.Width,
							firstRegion.Height
							);

						if (next == maxFramesInLine - 1)
						{
							line += 1;
							offset = maxFramesInLine * line;
						}
					}
				}
				else
				{
					throw new System.ArgumentOutOfRangeException("onerow");
				}
			}
			else
			{
				throw new System.ArgumentException("Must be greater than 0", (totalFrames > 0) ? "quantity" : "onerow");
			}
		}

		public TextureRegion(Texture2D texture, Point lefttop, Point rightbottom, int totalFrames, int maxFramesInLine, bool isHorizontal, int frameStep, int lineStep)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, rightbottom.X - lefttop.X, rightbottom.Y - lefttop.Y), totalFrames, maxFramesInLine, isHorizontal, frameStep, lineStep)
		{
		}

		public TextureRegion(Texture2D texture, Point lefttop, int width, int height, int totalFrames, int maxFramesInLine, bool isHorizontal, int frameStep, int lineStep)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, width, height), totalFrames, maxFramesInLine, isHorizontal, frameStep, lineStep)
		{
		}

		public TextureRegion(Texture2D texture, Rectangle firstRegion, int totalFrames, int maxFramesInLine, int frameStep, int lineStep)
			: this(texture, firstRegion, totalFrames, maxFramesInLine, true, frameStep, lineStep)
		{
		}

		public TextureRegion(Texture2D texture, Point lefttop, Point rightbottom, int totalFrames, int maxFramesInLine, int frameStep, int lineStep)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, rightbottom.X - lefttop.X, rightbottom.Y - lefttop.Y), totalFrames, maxFramesInLine, true, frameStep, lineStep)
		{
		}

		public TextureRegion(Texture2D texture, Point lefttop, int width, int height, int totalFrames, int maxFramesInLine, int frameStep, int lineStep)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, width, height), totalFrames, maxFramesInLine, true, frameStep, lineStep)
		{
		}

		public TextureRegion(Texture2D texture, Rectangle firstRegion, int totalFrames, Point frameStep)
			: this(texture)
		{
			if (totalFrames > 0)
			{
				this.frames = new Rectangle[totalFrames];
				this.frames[0] = firstRegion;

				//this.data = new Color[quantity][];
				//this.data[0] = new Color[firstRegion.Width * firstRegion.Height];
				//this.GetData(this.frames[0], 0);

				for (var i = 1; i < totalFrames; ++i)
				{
					this.frames[i] = new Rectangle(
						firstRegion.X + (i * frameStep.X),
						firstRegion.Y + (i * frameStep.Y),
						firstRegion.Width,
						firstRegion.Height);

					//this.data[i] = new Color[this.frames[i].Width * this.frames[i].Height];
					//this.GetData(this.frames[i], i);
				}
			}
			else
			{
				throw new System.ArgumentException("Must be greater than 0", "quantity");
			}
		}

		public TextureRegion(Texture2D texture, Point lefttop, int width, int height, int quantity, Point step)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, width, height), quantity, step)
		{
		}

		public TextureRegion(Texture2D texture, Point lefttop, Point rightbottom, int quantity, Point step)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, rightbottom.X - lefttop.X, rightbottom.Y - lefttop.Y), quantity, step)
		{
		}

		public TextureRegion(Texture2D texture, Rectangle region)
			: this(texture)
		{
			this.frames = new[] { region };

			//this.data = new Color[1][];
			//this.data[0] = new Color[region.Width * region.Height];
			//this.GetData(region, 0);
		}

		public TextureRegion(Texture2D texture, Point lefttop, int width, int height)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, width, height))
		{
		}

		public TextureRegion(Texture2D texture, Point lefttop, Point rightbottom)
			: this(texture, new Rectangle(lefttop.X, lefttop.Y, rightbottom.X - lefttop.X, rightbottom.Y - lefttop.Y))
		{
		}

		private TextureRegion(Texture2D texture)
		{
			if (texture != null)
			{
				this.texture = texture;
				this.currentFrame = 0;
			}
			else
			{
				throw new System.ArgumentNullException("texture");
			}
		}

		public Texture2D Texture
		{
			get { return this.texture; }
		}

		public Rectangle Region
		{
			get { return this.frames[this.currentFrame]; }
		}

		//public Color[] Data
		//{
		//	get { return this.data[this.currentFrame]; }
		//}

		public Rectangle[] Frames
		{
			get { return this.frames; }
		}

		public int Width
		{
			get { return this.frames[0].Width; }
		}

		public int Height
		{
			get { return this.frames[0].Height; }
		}

		public bool IsLast
		{
			get { return this.currentFrame == this.frames.Length - 1; }
		}

		/// <summary>
		/// Resets at the first frame.
		/// </summary>
		public void Reset()
		{
			this.currentFrame = 0;
		}

		/// <summary>
		/// Resets at the specific frame.
		/// </summary>
		/// <param name="index">Frame index.</param>
		public void Reset(int index)
		{
			this.currentFrame = (index > this.frames.Length - 1) ? this.frames.Length - 1 : index;
		}

		public void MoveNext()
		{
			this.MoveNext(false);
		}

		public void MoveNext(bool loop)
		{
			this.currentFrame += 1;
			if (this.currentFrame > this.frames.Length - 1)
			{
				this.currentFrame = loop ? 0 : this.frames.Length - 1;
			}
		}

		public Rectangle GetNextFrame()
		{
			return GetNextFrame(false);
		}

		public Rectangle GetNextFrame(bool loop)
		{
			this.MoveNext(loop);
			return this.frames[this.currentFrame];
		}

		//private void GetData(Rectangle region, int index)
		//{
		//	this.texture.GetData(0, region, this.data[index], 0, this.data[index].Length);
		//}
	}
}