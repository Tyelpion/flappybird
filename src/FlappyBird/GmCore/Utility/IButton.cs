﻿namespace GmCore
{
	using System;

	public delegate void HandlerClick(ButtonEventsArgs args);

	public interface IButton
	{
		event HandlerClick Click;

		void OnClicked();

		void UpdateVisualState();
	}

	public class ButtonEventsArgs : EventArgs
	{
		public ButtonEventsArgs(IButton sender)
		{
			this.Sender = sender;
		}

		public IButton Sender { get; private set; }
	}
}